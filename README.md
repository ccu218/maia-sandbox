# MAIA Sandbox - README #

This README documents steps necessary to get our application up and running.

### What is this repository for? ###

* Testing, training, experimenting with MAIA tech.
* 0.0
* [MAIA site](http://musicintelligence.co)

### How do I get set up? ###

* Install node & npm, and yarn
* Clone this repo
* Enter repo directory on the command line and run "yarn"
* run: npm start

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact