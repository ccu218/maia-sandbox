var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');

// var multer = require('multer'); // v1.0.5
// var upload = multer(); // for parsing multipart/form-data

var app = express();

// for parsing application/json
app.use(bodyParser.json());

// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// console logging
app.use(morgan('dev'));

// static files from the plublic directory
app.use(express.static(__dirname + '/public'));

app.get('/test',function(req,res){
  res.json({
    txt:'Hello World!'
  })
})

// get the environment port or set it if unset
var port = process.env.PORT || 9012;

// listen on that port
app.listen(port);

// let us know what port to dial
console.log('listening on http://localhost:'+port);
